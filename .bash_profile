#!/usr/bin/bash
################################################################################
# Copyright (C) 2021, 2023 ConsuLanza Informatica
# All rights reserved
# License: MIT
################################################################################
#
# set prompt
PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u:\[\033[01;34m\]\w\[\033[00m\]\$'
PATH=~/bin:${PATH}