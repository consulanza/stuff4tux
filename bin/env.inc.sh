#!/bin/bash
#
# impostazioni di base ambiente generico
#-----------------------------------------------------------------------------#
# carica i valori personalizzati per l'ambiente di sessione
#-----------------------------------------------------------------------------#
source ${HOME}/.env.sh
#-----------------------------------------------------------------------------#
# definisce le variabili di ambiente comuni per gli script
#-----------------------------------------------------------------------------#
BCK_DIR=${HOME}/var/backup
LOG_DIR=${HOME}/var/log
REMOTE_ACCESSES=${HOME}/etc/remote_user.dat
