# SSH

SSH ```Secure SHell```

## connessione shell a host remoto

```bash
[user@$host]$ ssh otheruser@host
```

Si collega utilizzando il protocollo SSH per aprire una shell sul sistema remoto ***host*** , presentandosi con
l'utente ***otheruser***.

in forma abbreviata:

```bash
[user@$host]$ ssh host
```

si collega utilizzando il protocollo SSH per aprire una shell sul sistema remoto ***host*** , presentandosi con il 
nome utente corrente, in questo caso ***user*** ed equivale a:

```bash
[user@$host]$ ssh user@host
```

## esecuzione comandi su remoto

Se oltre alle credenziali viene fornito un comando, questo verrà eseguito sul sistema remoto, girando l'output
sul sistema da cui è partita la connessione.

```bash
[user@host]$ ssh user@host "ls -Al"
```

produrrà l'elenco dei file remoti per l'utente ***user*** sul sistema ***host***.
Nello stesso modo può essere eseguita una procedura (script shell) che effettui elaborazioni specifiche, ad esempio
la generazione di un report.

## utilizzo sistema proxy

```bash
[user@host]$ ssh -o="ProxyJump=proxyuser@proxyrhost" anotheruser@anotherhost 
```

si collega al sistema ***anotherhost*** tramite un **tunnel ssh** fornito al sistema ***proxyhost***. Questo
viene normalmente utilizzato per definire un unico host (indirizzo IP) abilitato alla connessione sui sistemi
di produzione che funge da ponte fornendo un canale protetto e permettendo un maggiore controllo sugli accessi.

## sistemi per la gestione remota

La versatilità di SSH permette l'implementazione di sistemi per il controllo remoto che comprendono la possibilità di
aggiornamento, monitoraggio ed esecuzione.

### elementi per l'implementazione dei sistemi di controllo

<dl>
    <dt>chiavi crittografiche RSA</dt>
    <dd>Grazie alle chiavi RSA le procedure automatizzate possono accedere ai sistemi remoti senza dover richiedere le credenziali.</dd>
    <dt>tabella di associazione host e utenti</dt>
    <dd>Una tabella contenente le associazioni tra host remoti e nome utente, permette la creazione di menu interattivi per 
la selezione del sistema remoto su cui operare e può essere utilizzata dalle procedure per estrapolare il nome utente associato
in base all'host.</dd>
</dl>

### esempi 

#### verifica ed aggiornamento

Comprende i componenti:
<ul>
<li>script (remoto) per la verifica della versione corrente</li>
<li>script (locale) per l'esecuzione del remoto, verifica risultato ed eventuale invio della copia aggiornata</li>
<li>tabella di associazione host e username</li>
<li>menu (dialog) per la selezione della procedura e dell'host remoto</li>
</ul>

La procedura permette ad un operatore di far eseguire verifica e aggiornamento del sistema remoto da una postazione
centralizzata. La parte di interfaccia viene implementata utilizzando dialog, che estrae dalla tabella l'elenco dei
remoti quindi lancia lo script locale, passandogli le credenziali da utilizzare. Lo script locale provvede ad accedere
al remoto per acquisire i dati sulla versione e se necessario provvede all'aggiornamento.

### tecniche avanzate

Gli script possono essere strutturati in modo da integrare la parte locale e la parte remota; in base ad un test può
essere eseguita la parte interessata a seconda dell'host. In questo modo lo script stesso può essere in grado di
verificare l'esistenza e/o la versione sule remoto e, se necessario, inviare la copia di sè stesso sul remoto prima
di invocarne l'esecuzione.

**nota**
Gli script strutturati come descritto presentano anche un aspetto negativo di rischio: un errore sulla valutazione
dell'ambiente di esecuzione può portare a comportamenti disastrosi; ad esempio utilizzando questa tecnica per la
distribuzione del software e sbagliando il controllo, si rischia di creare un worm che in poco tempo si propaga
e prolifica su tutti gli host gestiti.