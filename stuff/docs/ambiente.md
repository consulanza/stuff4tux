# Ambiente di lavoro

L'ambiente di lavoro può essere personalizzato per facilitare le operazioni comuni. Il primo passo consiste nell'impostare
la sessione di lavoro tramite lo script di inizializzazione; nel caso di bash può essere [**.bashrc**](#bashrc) o **.bashprofile.**


## .bashrc

### impostazione del prompt

Il prompt può essere impostato per riflettere alcune informazioni, ad esempio utente e percorso corrente, un esempio è:

```bash
PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
```

nel file di profilo possiamo anche definire degli alias per i comandi più utilizzati, ad esempio:

```bash
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
```

## ambienti per il ciclo di vita del software

Per semplificare il ciclo di vita del software sarebbe opportuno utilizzare ambienti il più possibile omogenei
in modo da poter utilizzare le stesse soluzioni senza doverne ricreare in base all'ambiente (sistema operativo,
distribuzione Linux, hosting ecc.).

Per ovviare alle differenze è utile avere uno strato di gestione più generico ed astratto e l'impostazione dei
dettagli (come variabili) negli script di personalizzazione. Le procedure vanno quindi parametrizzate in modo 
da poter essere utilizzate in ambienti diversi senza ulteriori modifiche.

## gestione centralizzata

L'utilizzo di tecniche per l'esecuzione remota permette l'implementazione di un sistema per la gestione ed il
monitoraggio centralizzato. Il software per la gestione dell'ambiente dovrebbe poi sempre prevedere la possibilità di
essere aggiornato senza la necessità di collegarsi manualmente sul sistema.
