# Comandi base

| Comando                     | Funzione                                                             |
|-----------------------------|----------------------------------------------------------------------|
| [bash](#bash)               | bourne again shell                                                   |
| [ls](#ls)                   | elenco file                                                          |
| [cp](#cp)                   | copia file                                                           |
| [scp](#scp)                 | copia remota via tunnel SSH<br/>scp \<file sorgente> \<destinazione> |
| [rsync](#rsync)             | copia sincronizzata<br/>rsync \<sorgente> \<destinazione>            |
| [pwd](#pwd)                 | mostra directory corrente                                            |
| [cd](#cd)                   | cambia directory corrente                                            |
| [mv](#mv)                   | sposta (rinomina) file                                               |
| [ssh](#ssh)                 | secure shell                                                         |
| [ssh-keygen](#ssh-keygen)   | genera la chiave RSA per l'utente                                    |
| [ssh-copy-id](#ssh-copy-id) | copia la chiave RSA su un sistema remoto                             |
| [dialog](#dialog)           | utility Shell UI                                                     |
| [awk](#awk)                 | trattamento dati testuali                                            |
| [wget](#wget)               | effettua richieste http(s)                                           |
| [gzip](#gzip)               | compressore standard                                                 |
| [tar](#tar)                 | archiviatore standard                                                |
| [find](#find)               | ricerca file                                                         |
| [grep](#)                   | ricerca stringhe nei file con espressioni regolari         |



# bash

Shell per la linea comandi. In questi documenti facciamo riferimento a bash; a seconda dei sistemi e delle distribuzioni, la
sessione potrebbe essere avviata con un'altra (sh, zsh ecc), nel qual caso è possibile configurare il proprio account per 
utilizzarla come predefinita.

Gli script bash contengono la specifica per cui non richiedono necessariamente che la sessione 
utilizzi bash.


## pwd

***P**rint **W**orking **D**irectory*

Visualizza il percorso corrente 

<pre>
user@host:~$ pwd
/home/user
</pre>

## ls
*List*

Elenca i file contenuti nella directory corrente (vedi [pwd](#pwd)) o nel percorso specificato.

Le opzioni più utilizzate sono
<ul>
<li><strong>-a</strong> (mostra i [file nascosti](#file_nascosti)) compresi i file speciali '**.**' e '**..**'</li>
<li><strong>-A</strong> come -a ma non mostra i file speciali</li>
<li><strong>-l</strong> mosta la lista dettagliata</li>
</ul>

```bash
user@host:~$ ls
bin        CloudStorage  Documenti  Games      Immagini  Modelli  
Musica     Pubblici      Scaricati  Scrivania  snap      Video 
Workspace
```

```bash
user@host:~$ ls -al
totale 196
-rw-------  1 user group  2359 feb 16 23:58 .bash_history
-rw-r--r--  1 user group   220 feb  7 00:09 .bash_logout
-rw-r--r--  1 user group  3771 feb  7 00:09 .bashrc
drwxrwxr-x  3 user group  4096 feb  7 18:19 bin
drwxrwxr-x 22 user group  4096 feb 10 09:51 .cache
drwxrwxr-x  5 user group  4096 feb  7 23:28 CloudStorage
drwxr-xr-x 28 user group  4096 feb 17 15:01 .config
-rw-r--r--  1 user group    26 feb  7 00:23 .dmrc
drwxr-xr-x  6 user group  4096 feb 10 15:16 Documenti
```


## cp

**copy**

Copia file

```bash
cp <sorgente> <destinazione>
```

## mv

*MoVe*

mv permette lo spostamento (rinomina) dei file; a differenza del comando rename (ren) di altri sistemi, se la destinazione
è su un device differente, sposta effettivamente il file invece di copiarlo semplicemente.

```bash
mv <sorgente> <destinazione>
```

## scp

permette la copia tra sistemi differenti utilizzando un tunnel ssh

## rsync

Permette la sicronizzazione di file e directory in modo ricorsivo. Offre notevoli utilità:
<ul>
<li>copia solamente i file che risultano più recenti di quelli sulla destinazione</li>
<li>utilizza ssh quindi la trasmissoine è crittografata</li>
<li>può comprimere al volo i file per ridurre il consumo di banda</li>
</ul>

## ssh

***S****ecure* ***SH****ell*

Ssh permette di stabilire un canale di cumunicazione sicuro e versatile tra due punti; viene comunemente
utilizzato per creare dei tunnel protetti tramite cui accedere a sistemi remoti. Tra i vari utilizzi troviamo:
<ul>
<li>accesso shell remoto</li>
<li>copia di file /scp, rsync)</li>
<li>esecuzione comandi su sistemi remoti</li>
</ul>

L'utilizzo più comune è per la connessione ad host remoti
```bash
ssh user@host
```

Con SSH è possibile impostare un sistema di controllo centralizzato che effettua sui sistemi remoti tutte le operazioni
per cui sarebbe altrimenti necessario collegarsi sul remoto.

Per semplificare l'autenticazione si possono utilizzare le chiavi di crittografia RSA tramite i due comandi
descritti qui sotto.:

### ssh-keygen
```bash
ssh-keygen
```

per generale la propria chiave; questa operazione va effettuata una volta sola; creerà le chiavi nella directory .ssh:
<ul>
    <li>id_rsa: chiave privata che va tenuta segreta e conservata con cura</li>
    <li>id_rda.pub: chiave pubblica che potrà essere distribuita con **ssh-copy-id**</li>
</ul>

### ssh-copy-id

copia la chiave pubblica sul sistema remoto

```bash
ssh-copy-id -i /.ssh/id_rsa.pub user@host
```
Dopo aver effettuato la copia della chiave pubblica sul sistema remoto, sarà possibile effettuare la connessione ssh
con l'autenticazione fornita dalla chiave.

```bash
ssh user@host
```

Attenzione a non specificare la chiave private (id_rsa)

## dialog

Dialog permette la creazione di script shell con interfaccia utente in modalità caratteri. Con dialog si possono
creare finestre di dialogo, menu, richieste di input e altro, rendendo interattivi gli script e facilitandone
l'utilizzo da parte degli operatori. Ad esempio si possono creare finestre dinamiche di riepilogo (elenco file salvati)
o di selezione file (file da salvare o cancellare) oltre a poter lanciare qualsiasi altra operazione.

## awk

Awk è un linguaggio orientato alla manipolazione di dati testuali. La facilità nel definire dei pattern da ricercare
lo rende estremamente utile per l'analisi di file di log e la produzione di report o per fornire filtri su qualsiasi
flusso di testo.

Un utilizzo efficace potrebbe consistere nell'esame dei log per smistare le segnalazioni alle entità interessate.

## wget

Wget effettua chiamate http(s), può essere utilizzato per scaricare pagine e archivi inoltre ha la possibilità di recuperare
ricorsivamente il contenuto di un sito (solo le pagine e ciò che sia visibile via web).

## gzip

***G****NU* ***Zip***

Compressore standard ampiamente utilizzato, tra cui per:
<ul>
<li>Compressione di file e archivi</li>
<li>Compressione flusso dati per rsync e http</li>
</ul>

## tar

***T****ape* ***AR****chiver*

Utility di backup per la creazione di archivi. Può utilizzare gzip per creare archivi compressi (normalmente .tar.gz o .tgz).

## find

Find è uno strumento molto versatile per la ricerca di file e l'eventuale esecuzione di comandi su di essi.
Permette di specificare il tipo, la data, il nome e altri valori per gli attributi dei file da cercare

## grep

Ricerca stringhe nei file. Utilizza espressioni regolari per cercare le stringhe.