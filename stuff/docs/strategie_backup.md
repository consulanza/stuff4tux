# Strategie di backup #

I backup sono fondamentali per la sicurezza dei sistemi, qui affronteremo gli aspetti principali per implementare una
strategia di backup efficace.
Per semplicità ci riferiremo a generici ***dati*** in quanto a una procedura non interessa se nello specifico siano
da salvare file, database, librerie o altro. 

## Obiettivi ##

L'obiettivo principale dei backup è la salvaguardia del sistema e dei dati tramite procedure che effettuino il
salvataggio dei dati e ne permettano il ripristino dei dati nel modo più semplice e veloce possibile.

## Automazione ##

La creazione di procedure di salvataggio standardizzate ci permette di implementare delle strategie
precise, semplificando la creazione delle corrispettive procedure di ripristino. 

## Caratteristiche volute ##

Come accennato, uno dei primi requisiti richiesti è la standardizzazione delle procedure; questo significa che le stesse
vanno create in modo che siano adeguabili ad ambienti differenti semplicemente variando dei parametri ma mantenendo
lo stesso codice su sistemi e per applicazioni differenti. In questo modo il trasporto delle procedure da un sistema
all'altro non richiederà particolari attenzioni se non per l'impostazione corretta dei parametri, la quantità di codice
non aumenterà spropositatamente e la manutenzione sarà più semplice.

## Gestione del codice ##

Possiamo pensare il sistema di backup come a una qualsiasi altra applicazione e applicare gli stessi principi.
Il codice dovrà quindi essere mantenuto in un repository GIT mentre i sistemi che lo dovranno utilizzare potranno 
essere mantenuti aggiornati tramite clone e update.

## Ambiente e parametri ##

Per permettere la standardizzazione e la parametrizzazione abbiamo la necessità di definire un ambiente di lavoro
entro cui le nostre applicazioni possano lavorare ignorando le differenze specifiche poiché le stesse vengono gestite
da quello che chiameremo ***software d'ambiente***. 
Il software d'ambiente aggiunge uno strato che si occupa degli aspetti variabili come il nome host, le credenziali,
percorsi dei file, la destinazione dei backup e cos' via.

## Gestione centralizzata ##

Un buon sistema di backup deve permettere monitoraggio e gestione sia direttamente sui sistemi che da un sistema
centralizzato. L'eventuale impossibilità a operare in uno dei due modi non deve mai essere un limite tecnico ma un
preciso blocco indipendentemente dalle possibilità tecniche delle procedure, che deve poter essere abilitato e/o
disabilitato senza modificare il codice ma semplicemente impostando dei parametri o con azioni esterne come disabilitare
l'accesso utente. Questo significa che un buon sistema di backup può essere utilizzato sia sul proprio PC (in modalità 
standalone) che sui sistemi di sviluppo e produzione tramite automazione e controllo remoto.

Il sistema centrale può tenere traccia dei sistemi remoti utilizzando delle tabelle che riportino i dati essenziali come
il nome host e l'utente da utilizzare per l'accesso e le operazioni sui sistemi di produzione. Può inoltre effettuare il
setup del software d'ambiente senza richiedere l'accesso al sistema remoto da parte dell'operatore nel seguente modo:

- inserimento delle credenziali nella tabella dei sistemi
- copia della chiave RSA dal sistema monitor al nuovo sistema di produzione
- esecuzione remota (tramite SSH) della procedura per la creazione della struttura e il prelievo del codice da GIT

## Esecuzione delle procedure sui sistemi remoti ##

Le procedure devono essere eseguibili sia lanciandole in locale che tramite SSH dal sistema di monitoraggio esterno, in
modo che sia possibile lanciare quelle ordinarie tramite lo schedulatore locale e quelle straordinarie in modo manuale
(indifferentemente da locale o da remoto).

### Avvio esecuzione da sistema centrale ###

Per semplificare la gestione di più sistemi possiamo appoggiarci a una tabella contenente le informazioni necessarie per
l'accesso ai sistemi remoti, qualcosa del tipo:

| applicazione | ambiente   | host                | username      |
|--------------|------------|---------------------|---------------|
| ebookshop    | develop    | ebook.devdomain.it  | ebookshop     |
| ebookshop    | test       | ebook.testdomain.it | ebookshop     |
| ebookshop    | production | ebook.com           | ebookshop     |
| crappy       | develop    | crappy.pinco.com    | tizio         |
| crappy       | test       | test.crappy.com     | caio          |
| crappy       | production | crappy.com          | crappy_826348 |

grazie alla tabella il sistema centrale sa come accedere ai remoti e all'operatore sarà necessario solamente sapere
su quale applicazione e ambiente far eseguire la procedura remota. Per semplificare ulteriormente la vita e migliorare
la sicurezza, l'impostazione delle chiavi RSA per SSH renderà trasparente l'accesso remoto e non richiederà la
diffusione delle password a tutti gli operatori; aumentando così i livello di sicurezza.

## Verifica risultati operazioni e stato del sistema ##

Per la verifica dei risultati ci sono due possibilità: ricevere una notifica o andare attivamente a prelevarli sul
sistema interessato. La prima soluzione è quella più comoda e più efficace in quanto

- il sistema monitor non necessita l'utilizzo di tabelle per accedere al remoto
- l'esecuzione delle procedure di backup rimane completamente asincrona rispetto alle operazioni sul sistema monitor
- il sistema monitor non deve preoccuparsi di elenchi e credenziali in quanto riceve le informazioni direttamente dai remoti
- le informazioni di riepilogo arrivano sul sistema monitor in tempo reale
- grazie alla posta elettronica o sistemi di *message queue*, possono essere utilizzati più sistemi di monitoraggio
che lavorano un concomitanza senza doversi preoccupare di conflitti nell'attivazione di procedure remote
- l'aggiunta di sistemi di monitoraggio può essere semplificata impostando un indirizzo alias (lista di distribuzione)
nel caso della posta elettronica o effettuando la sottoscrizione del sistema monito alla coda messaggi interessata.




