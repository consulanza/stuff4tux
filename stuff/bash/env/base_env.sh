#!/bin/bash
################################################################################
# Copyright (C) 2021, 2023 ConsuLanza Informatica
# All rights reserved
# License: MIT
################################################################################
#
# load environment variables
#
source ~/env.sh
#
function setPhpPlesk() {
  if [ -f /opt/plesk/php/7.1/bin/php ]; then
      PHP71='/opt/plesk/php/7.1/bin/php'
  fi
  if [ -f /opt/plesk/php/7.2/bin/php ]; then
      PHP72='/opt/plesk/php/7.2/bin/php'
  fi
  if [ -f /opt/plesk/php/7.3/bin/php ]; then
      PHP73='/opt/plesk/php/7.3/bin/php'
  fi
  if [ -f /opt/plesk/php/7.4/bin/php ]; then
      PHP74='/opt/plesk/php/7.4/bin/php'
  fi
  if [ -f /opt/plesk/php/8.0/bin/php ]; then
      PHP80='/opt/plesk/php/8.0/bin/php'
  fi
  if [ -f /opt/plesk/php/8.1/bin/php ]; then
      PHP81='/opt/plesk/php/8.1/bin/php'
  fi

  case ${PREFERRED_PHP} in
    71)
      PHP_EXE=${PHP71}
      ;;
    72)
      PHP_EXE=${PHP72}
      ;;
    73)
      PHP_EXE=${PHP73}
      ;;
    74)
      PHP_EXE=${PHP74}
      ;;
    80)
      PHP_EXE=${PHP80}
      ;;
    81)
      PHP_EXE=${PHP81}
      ;;
  esac
}
#
function setPhpLocal() {
  PHP_EXE=$(which php)
}
#
function setup_plesk() {
  setPhpPlesk
}

function setup_azure() {
  setPhpAzure
}
#
function setup_local() {
  setPhpLocal
}
#
function setup_ispconfig() {
  setPhpLocal
}

function setup_cyberpanel() {
  PHP_EXE=/usr/local/lsws/lsphp${PREFERRED_PHP}/bin/php
}
#
function setPhpAzure() {
  setPhpLocal
}
#
case ${HOSTING_TYPE} in
  plesk)
    setup_plesk
    ;;
  azure)
    setup_azure
    ;;
  ispconfig)
    setup_azure
    ;;
  cyberpanel)
    setup_cyberpanel
    ;;
  cloudable)
    setup_local
    ;;
  *)
    setup_local
    ;;
esac

function setupWebroot() {
# WEB ROOT può variare fra Plesk/Azure
#
# ENVROOT definito in env.sh
#
  if [ -d "${ENVROOT}/htdocs" ]; then
      WEBROOT="${ENVROOT}/htdocs"
  elif [ -d "${ENVROOT}/httpdocs" ]; then
      WEBROOT="${ENVROOT}/httpdocs"
  elif [ -d "${ENVROOT}/html" ]; then
      WEBROOT="${ENVROOT}/html"
  elif [ -d "${ENVROOT}/web" ]; then
      WEBROOT="${ENVROOT}/web"
  elif [ -d "${ENVROOT}/public_html" ]; then
      WEBROOT="${ENVROOT}/public_html"
  elif [ -d "${ENVROOT}/website-prod/www" ]; then
      WEBROOT="${ENVROOT}/website-prod/www"
  else
    echo "ATTENZIONE:\n"
    echo "La variabile WEBROOT non è stata definita,"
    echo "possibili problemi con le utility Magento."
  fi
}
#
PHP="${PHP_EXE} -dmemory_limit=-1"
setupWebroot
#

