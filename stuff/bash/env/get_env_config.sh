#!/bin/bash
################################################################################
# Copyright (C) 2021, 2023 ConsuLanza Informatica
# All rights reserved
# License: MIT
################################################################################
#
source ~/env.sh
source ~/bin/env/base_env.sh
source ~/bin/env/common_defines.sh
#
function get_env_config_values() {
 local values=(
"PROJECTNAME=${PROJECTNAME};\
ENV_TYPE=${ENV_TYPE};\
DEF_GIT=${DEF_GIT};\
LANG_CODES=$(echo ${LANG_CODES} | sed -r 's/ /,/g');\
HOSTING_TYPE=${HOSTING_TYPE};\
PREFERRED_PHP=${PREFERRED_PHP};\
ENVROOT=${ENVROOT};\
WEBROOT=${WEBROOT};\
WEB_URL=${WEB_URL};\
WEBAPP=${WEBAPP};\
WEBLOG=${WEBLOG};\
WEBIMPORT=${WEBIMPORT};\
PHP=${PHP};\
COMPOSER=${COMPOSER};\
MAGE=${MAGE};
")
  echo $values
}
get_env_config_values
