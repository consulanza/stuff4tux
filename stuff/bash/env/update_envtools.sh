#!/bin/bash
################################################################################
# Copyright (C) 2021, 2023 ConsuLanza Informatica
# All rights reserved
# License: MIT
################################################################################
CURDIR=$(/usr/bin/pwd)
cd ${HOME}/bin
git checkout master
git pull