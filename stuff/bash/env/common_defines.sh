#!/bin/bash
################################################################################
# Copyright (C) 2021, 2023 ConsuLanza Informatica
# All rights reserved
# License: MIT
################################################################################
#
source ~/env.sh
source ~/bin/env/base_env.sh
#
COMPOSER2="${PHP} $HOME/bin/develop/composer2.phar"
COMPOSER="${PHP} $HOME/bin/develop/composer.phar"
N98MAGERUN="${PHP} $HOME/bin/mage/n98-magerun2.phar"
MAGE="${PHP} ${WEBROOT}/bin/magento"
#
WEBAPP="${WEBROOT}/app"
WEBLOG="${WEBROOT}/var/log"
WEBIMPORT="${WEBROOT}/var/import"
#
# definisce l'url del website per il progetto
# per ambienti non standard va definito nel file env.sh altrimenti
# qui viene definito in base alla nomeclatura standar per gli ambienti di sviluppo
#
if [ "${WEB_URL}" == "" ]
then
  WEB_URL="https://${ENV_TYPE}.${PROJECTNAME}.ne-ws.it"
fi