#!/bin/bash
################################################################################
# Copyright (C) 2021, 2023 ConsuLanza Informatica
# All rights reserved
# License: MIT
################################################################################
# clear session files
source ~/env.sh
source ~/bin/env/base_env.sh
source ~/bin/env/common_defines.sh
#
SESSIONS_DIR='${SITEROOT}/sessions'
/usr/bin/find ${SESSIONS_DIR} -mmin +2880 -type f -name sess_* -exec rm -f {} \;
echo "terminato"
echo ""
