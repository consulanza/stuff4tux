#!/bin/bash
################################################################################
# Copyright (C) 2021, 2023 ConsuLanza Informatica
# All rights reserved
# License: MIT
################################################################################
source ~/env.sh
source ~/bin/env/base_env.sh
source ~/bin/env/common_defines.sh
#
#
#Num  Colour    #define         R G B
#
#0    black     COLOR_BLACK     0,0,0
#1    red       COLOR_RED       1,0,0
#2    green     COLOR_GREEN     0,1,0
#3    yellow    COLOR_YELLOW    1,1,0
#4    blue      COLOR_BLUE      0,0,1
#5    magenta   COLOR_MAGENTA   1,0,1
#6    cyan      COLOR_CYAN      0,1,1
#7    white     COLOR_WHITE     1,1,1
#
#tput bold    # Select bold mode
#tput dim     # Select dim (half-bright) mode
#tput smul    # Enable underline mode
#tput rmul    # Disable underline mode
#tput rev     # Turn on reverse video mode
#tput smso    # Enter standout (bold) mode
#tput rmso    # Exit standout mode
#

# Foreground (text) colors
FG_BLACK=$(tput setaf 0)
FG_RED=$(tput setaf 1)
FG_GREEN=$(tput setaf 2)
FG_YELLOW=$(tput setaf 3)
FG_BLUE=$(tput setaf 4)
FG_MAGENTA=$(tput setaf 5)
FG_CYAN=$(tput setaf 6)
FG_WHITE=$(tput setaf 7)

# Background colors
BG_BLACK=$(tput setab 0)
BG_RED=$(tput setab 1)
BG_GREEN=$(tput setab 2)
BG_YELLOW=$(tput setab 3)
BG_BLUE=$(tput setab 4)
BG_MAGENTA=$(tput setab 5)
BG_CYAN=$(tput setab 6)
BG_WHITE=$(tput setab 7)

# Character attibutes
ATTR_BOLD=$(tput bold)
ATTR_HIGH=$(tput sitm)
ATTR_NORMAL=$(tput sgr 0)
#
LINE_TAB="\t\t"
LINE_FILL="                   "
EMPTY_DESC="                 "
ENV_TITLE=" Sei in ambiente "

set_envtype_desc() {

  case ${check_env} in
    wrk)
        ENV_DESC="  Working copy   "
        BGCOLOR=${ATTR_NORMAL}
        FGCOLOR=${FG_GREEN}
      ;;
    dev)
        ENV_DESC="   Development   "
        BGCOLOR=${BG_GREEN}
        FGCOLOR=${FG_WHITE}
      ;;
    stg)
        ENV_DESC="     Staging     "
        BGCOLOR=${BG_YELLOW}
        FGCOLOR=${FG_RED}
      ;;
    pro)
        ENV_DESC="    Production   "
        BGCOLOR=${BG_RED}
        FGCOLOR=${FG_WHITE}
      ;;
    man)
        ENV_DESC="   Manutenzione  "
        BGCOLOR=${BG_RED}
        FGCOLOR=${FG_YELLOW}
      ;;
    *)
        ENV_DESC="   Sconosciuto   "
        BGCOLOR=${BG_YELLOW}
        FGCOLOR=${FG_RED}
      ;;
  esac
}
#
if [ ! -z $1 ]; then
  check_env=$1
else
  check_env=$ENV_TYPE
fi
#
set_envtype_desc $check_env
#
echo -e ""
echo -e "${LINE_TAB}${LINE_FILL}${FGCOLOR}${ATTR_BOLD}${ATTR_HIGH}${PROJECTNAME} ${ENV_TYPE}${LINE_FILL}${ATTR_NORMAL}"
echo -e ""
echo -e "${LINE_TAB}${BGCOLOR}${FGCOLOR}${LINE_FILL} ${EMPTY_DESC} ${LINE_FILL}${ATTR_NORMAL}"
echo -e "${LINE_TAB}${BGCOLOR}${FGCOLOR}${LINE_FILL} ${EMPTY_DESC} ${LINE_FILL}${ATTR_NORMAL}"
echo -e "${LINE_TAB}${BGCOLOR}${FGCOLOR}${LINE_FILL} ${EMPTY_DESC} ${LINE_FILL}${ATTR_NORMAL}"
echo -e "${LINE_TAB}${BGCOLOR}${FGCOLOR}${LINE_FILL} ${ENV_TITLE} ${LINE_FILL}${ATTR_NORMAL}"
echo -e "${LINE_TAB}${BGCOLOR}${FGCOLOR}${LINE_FILL} ${EMPTY_DESC} ${LINE_FILL}${ATTR_NORMAL}"
echo -e "${LINE_TAB}${BGCOLOR}${FGCOLOR}${LINE_FILL}${ATTR_BOLD}${ATTR_HIGH} ${ENV_DESC} ${LINE_FILL}${ATTR_NORMAL}"
echo -e "${LINE_TAB}${BGCOLOR}${FGCOLOR}${LINE_FILL} ${EMPTY_DESC} ${LINE_FILL}${ATTR_NORMAL}"
echo -e "${LINE_TAB}${BGCOLOR}${FGCOLOR}${LINE_FILL} ${EMPTY_DESC} ${LINE_FILL}${ATTR_NORMAL}"
echo -e "${LINE_TAB}${BGCOLOR}${FGCOLOR}${LINE_FILL} ${EMPTY_DESC} ${LINE_FILL}${ATTR_NORMAL}"
echo -e ""
