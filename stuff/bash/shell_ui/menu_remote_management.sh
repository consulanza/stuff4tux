#!/bin/bash
################################################################################
# Copyright (C) 2021 ConsuLanza Informatica
# All rights reserved
################################################################################
#
# Remote management menu
#
SCRIPTDIR="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
source ${SCRIPTDIR}/dlg_functions.inc.sh
source ${SCRIPTDIR}/remote_mage_functions.inc.sh
source ${SCRIPTDIR}/remote_composer_functions.inc.sh
source ${SCRIPTDIR}/remote_env_functions.inc.sh
source ${SCRIPTDIR}/remote_m2utils_functions.inc.sh
#
#===============================================================================
# MAGENTO 2
#===============================================================================
#
#-------------------------------------------------------------------------------
# magento 2 functions menu
# $1 env
# $2 user
#-------------------------------------------------------------------------------
function rmt_menu_magento_2() {
  local exitStatus=${DIALOG_OK};
  local choice
  local MENUHEIGHT=0
  local MENUWIDTH=0
  local MENULINES=25
  #
  choice=$(dialog \
	  --backtitle "Gestione Magento 2 remoto [$1] - [$2]" \
		--title "Gestione [$1] " \
		--ok-label "Seleziona" \
		--cancel-label "Esci" \
		--menu "Seleziona l'operazione Magento su remoto\n ambiente ${REMOTE_ENV}\n utente   ${REMOTE_USR}" ${MENUHEIGHT} ${MENUWIDTH} ${MENULINES} \
		1 "Full rebuild (mage_all)" \
		2 "Rebuild (upgrade & compile)" \
		3 "Deploy static contents" \
		4 "Clear cache" \
		5 "Clear log" \
		6 "Reindex all" \
		7 "Enable maintenance mode" \
		8 "Disable maintenance mode" \
		9 "Show Magento version" \
		a "Show modules status" \
		b "Show cache status" \
		c "Deploy application (git pull)" \
		d "Composer update" \
		e "Composer install" \
		f "Installa Magento 2 standard" \
		g "Installa personalizzazioni Magento 2 standard" \
		3>&2 2>&1 1>&3) # show dialog and store output
	#
	exitStatus=$?
	if [ "$exitStatus" == $DIALOG_OK ]
	then
	  echo $choice
	else
	  echo 0
	fi
	return $exitStatus;
}
#-------------------------------------------------------------------------------
# magento 2 functions execution
# $1 env
# $2 user
# $3 choice
#-------------------------------------------------------------------------------
function rmt_menu_magento_2_execution() {
    case $3 in
    1) remote_mage_all "$1" "$2"
      ;;
    2) remote_mage_rebuild "$1" "$2"
      ;;
    3) remote_mage_deploy "$1" "$2"
      ;;
    4) remote_mage_clearcache "$1" "$2"
      ;;
    5) remote_mage_clearlog "$1" "$2"
      ;;
    6) remote_mage_reindex "$1" "$2"}
      ;;
    7) remote_mage_maintenance_on "$1" "$2"
      ;;
    8) remote_mage_maintenance_on "$1" "$2"
      ;;
    9) remote_mage_show_version "$1" "$2"
      ;;
    a) remote_mage_show_modules "$1" "$2"
      ;;
    b) remote_mage_show_cache "$1" "$2"
      ;;
    c) remote_mage_deploy "$1" "$2"
      ;;
    d) remote_composer_update "$1" "$2"
      ;;
    e) remote_composer_install "$1" "$2"
      ;;
    f) remote_mage_install_standard_m2 "$1" "$2"
      ;;
    g) remote_mage_install_standard_customization "$1" "$2"
      ;;
  esac
}
#
#-------------------------------------------------------------------------------
# magento 2 functions menu loop
# $1 env
# $2 user
#-------------------------------------------------------------------------------
function rmt_menu_magento_2_loop() {
  local exitStatus=${DIALOG_OK};
  local choice
  #
  while [ "$exitStatus" == ${DIALOG_OK} ]; do
    # remote access menu
    choice=$(rmt_menu_magento_2 "$1" "$2")
    exitStatus=$?
    if [ "$exitStatus" == ${DIALOG_OK} ];
    then
      rmt_menu_magento_2_execution $1 $2 $choice
    fi
  done
  return $exitStatus
}
#
#===============================================================================
# M2 Utils
#===============================================================================
#
#-------------------------------------------------------------------------------
# magento 2 functions menu
# $1 env
# $2 user
#-------------------------------------------------------------------------------
function rmt_menu_m2utils() {
  local exitStatus=${DIALOG_OK};
  local choice
  local MENUHEIGHT=0
  local MENUWIDTH=0
  local MENULINES=25
  #
  choice=$(dialog \
	  --backtitle "Gestione M2Utils remoto [$1] - [$2]" \
		--title "Gestione [$1] " \
		--ok-label "Seleziona" \
		--cancel-label "Esci" \
		--menu "Seleziona l'operazione su remoto\n ambiente ${REMOTE_ENV}\n utente   ${REMOTE_USR}" ${MENUHEIGHT} ${MENUWIDTH} ${MENULINES} \
		1 "Mostra configurazione ambiente" \
		2 "Aggiorna M2 Utils" \
		3 "Installa M2 Utils" \
		3>&2 2>&1 1>&3) # show dialog and store output
	#
	exitStatus=$?
	if [ "$exitStatus" == $DIALOG_OK ]
	then
	  echo $choice
	else
	  echo 0
	fi
	return $exitStatus
}
#-------------------------------------------------------------------------------
# M2 Utils functions execution
# $1 env
# $2 user
# $3 choice
#-------------------------------------------------------------------------------
function rmt_menu_m2utils_execution() {
    case $3 in
    1) remote_env_get_config "$1" "$2"
      ;;
    2) remote_m2utils_update "$1" "$2"
      ;;
    3) remote_m2utils_install "$1" "$2"
      ;;
  esac
}
#-------------------------------------------------------------------------------
# M2 Utils functions menu loop
# $1 env
# $2 user
#-------------------------------------------------------------------------------
function rmt_menu_m2utils_loop() {
  local exitStatus=${DIALOG_OK};
  local choice
  #
  while [ "$exitStatus" == ${DIALOG_OK} ]; do
    choice=$(rmt_menu_m2utils "$1" "$2")
    exitStatus=$?
    if [ "$exitStatus" == ${DIALOG_OK} ];
    then
      rmt_menu_m2utils_execution "$1" "$2" $choice
    fi
  done
  return $exitStatus
}
#
#===============================================================================
# Miscellanea
#===============================================================================
#
#-------------------------------------------------------------------------------
# magento 2 functions menu
# $1 env
# $2 user
#-------------------------------------------------------------------------------
function rmt_menu_misc() {
  local exitStatus=${DIALOG_OK};
  local choice
  local MENUHEIGHT=0
  local MENUWIDTH=0
  local MENULINES=25
  #
  choice=$(dialog \
	  --backtitle "Gestione remoto [$1] - [$2]" \
		--title "Gestione [$1] " \
		--ok-label "Seleziona" \
		--cancel-label "Esci" \
		--menu "Seleziona l'operazione su remoto\n ambiente ${REMOTE_ENV}\n utente   ${REMOTE_USR}" ${MENUHEIGHT} ${MENUWIDTH} ${MENULINES} \
		1 "Mostra chiave SSH pubblica" \
		2 "Invia chiave SSH (ssh-copy-id)" \
		3 "Genera chiave SSH (ssh-keygen)" \
		3>&2 2>&1 1>&3) # show dialog and store output
	#
	exitStatus=$?
	if [ "$exitStatus" == $DIALOG_OK ]
	then
	  echo $choice
	else
	  echo 0
	fi
	return $exitStatus
}
#-------------------------------------------------------------------------------
# M2 Utils functions execution
# $1 env
# $2 user
# $3 choice
#-------------------------------------------------------------------------------
function rmt_menu_misc_execution() {
  case $3 in
    1) remote_env_show_id_rsa "$1" "$2"
      ;;
    2) remote_env_ssh_copy_id "$1" "$2"
      ;;
    3) remote_env_ssh_keygen "$1" "$2"
      ;;
  esac
}
#
#-------------------------------------------------------------------------------
# Miscellanea functions menu loop
# $1 env
# $2 user
#-------------------------------------------------------------------------------
function rmt_menu_misc_loop() {
  local exitStatus=${DIALOG_OK};
  local choice
  #
  while [ "$exitStatus" == ${DIALOG_OK} ]; do
    choice=$(rmt_menu_misc "$1" "$2")
    exitStatus=$?
    if [ "$exitStatus" == ${DIALOG_OK} ];
    then
      rmt_menu_misc_execution "$1" "$2" $choice
    fi
  done
  return $exitStatus
}
#
#===============================================================================
# first level
#===============================================================================
#
#-------------------------------------------------------------------------------
# first level menu
# $1 env
# $2 user
#-------------------------------------------------------------------------------
function rm_1stlevel_menu() {
  local exitStatus=${DIALOG_OK};
  local choice
  local MENUHEIGHT=0
  local MENUWIDTH=0
  local MENULINES=25
  #
  choice=$(dialog \
	  --backtitle "Gestione ambienti remoti [$1] - [$2]" \
		--title "Gestione [$1] " \
		--ok-label "Seleziona" \
		--cancel-label "Esci" \
		--menu "Seleziona sezione" ${MENUHEIGHT} ${MENUWIDTH} ${MENULINES} \
		1 "Gestione Magento 2" \
		2 "Gestione Utility M2" \
		3 "Altro" \
		3>&2 2>&1 1>&3) # show dialog and store output
	#
	exitStatus=$?
	if [ "$exitStatus" == $DIALOG_OK ]
	then
	  echo $choice
	else
	  echo 0
	fi
	return $exitStatus;
}
#-------------------------------------------------------------------------------
# first level menu choice execution
# $1 env
# $2 user
# $3 choice
#-------------------------------------------------------------------------------
function rm_1stlevel_menu_execution() {
  case $3 in
    1) rmt_menu_magento_2_loop "$1" "$2"
      ;;
    2) rmt_menu_m2utils_loop "$1" "$2"
      ;;
    3) rmt_menu_misc_loop "$1" "$2"
      ;;
  esac
}
#-------------------------------------------------------------------------------
# first level menu loop
# $1 env
# $2 user
#-------------------------------------------------------------------------------
function rm_1stlevel_menu_loop() {
  local exitStatus=${DIALOG_OK};
  local choice
  #
  while [ "$exitStatus" == ${DIALOG_OK} ]; do
    # remote access menu
    choice=$(rm_1stlevel_menu "$1" "$2")
    exitStatus=$?
    if [ "$exitStatus" == ${DIALOG_OK} ];
    then
      rm_1stlevel_menu_execution "$1" "$2" $choice
    fi
  done
  return $exitStatus
}
#
#===============================================================================
# MAIN
#===============================================================================
#
#-------------------------------------------------------------------------------
# main loop
#-------------------------------------------------------------------------------
function menu_select_remote() {
  local selected_row=""
  local exitStatus=0
  selected_row=$(show_ssh_access_menu "Gestione ambiente remoto" ${NO_CONFIRM_REQUIRED})
  exitStatus=$?
  if [ $exitStatus == ${DIALOG_OK} ]
  then
    echo $selected_row
  else
    echo ""
  fi
  return $exitStatus
}
#
#-------------------------------------------------------------------------------
# main loop
#-------------------------------------------------------------------------------
function rm_main_menu_loop() {
  local exitStatus=${DIALOG_OK};
  local choice

  while [ "$exitStatus" == ${DIALOG_OK} ]; do
    # remote access menu
    choice=$(menu_select_remote)
    exitStatus=$?
    if [ "$exitStatus" == ${DIALOG_OK} ];
    then
      local oldIFS="$IFS"
      IFS=','
      selected_value=($choice)
      IFS=$oldIFS
      REMOTE_ENV="${selected_value[0]}"
      REMOTE_USR="${selected_value[1]}"
      #
      rm_1stlevel_menu_loop "${REMOTE_ENV}" "${REMOTE_USR}"
    fi
  done
  clear
  return $exitStatus
}
#===============================================================================
# main
#===============================================================================
rm_main_menu_loop