#!/bin/bash
#------------------------------------------------------------------------------
# Save important data to local cloud storage directory
# Copyright (C) 2020-2023 Amedeo Lanza - ConsuLanza Informatica
# License: GPLv3
#
# Questa procedura effettua il salvataggio di alcuni file importanti su
# Dropbox e Mega.
#
#------------------------------------------------------------------------------
#
MEGAINSTANCE="mymegaaccount.mydomain.it"
DROPBOX="${HOME}/CloudStorage/Dropbox"
MEGASYNC="${HOME}/CloudStorage/Mega/${MEGAINSTANCE}"
#
echo "Copia Keepass"
#
rsync -a ~/Documenti/Keepass/*.kdbx	${DROPBOX}/Documenti/Keepass/
rsync -a ~/Documenti/Keepass/*.kdbx	${MEGASYNC}/BACKUP/Keepass/
#
echo "Copia Utility"
#
rsync -ar ~/bin/*   ${DROPBOX}/bin/
rsync -ar ~/bin/*		${MEGASYNC}/BACKUP/bin/
#
echo "Copia modelli"
#
rsync -avr ~/Modelli/*			${DROPBOX}/Modelli/
rsync -avr ~/Modelli/*			${MEGASYNC}/BACKUP/Modelli/
