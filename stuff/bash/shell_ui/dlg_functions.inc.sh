#!/bin/bash
################################################################################
# Copyright (C) 2021 ConsuLanza Informatica
# All rights reserved
################################################################################
#
# standard utility functions for dialog-enabled scripts
#
# Define the dialog exit status codes
#
function setup_dialog_codes() {
  : ${DIALOG_OK=0}
  : ${DIALOG_CANCEL=1}
  : ${DIALOG_HELP=2}
  : ${DIALOG_EXTRA=3}
  : ${DIALOG_ITEM_HELP=4}
  : ${DIALOG_ESC=255}
  #
  : ${CONFIRM_REQUIRED=1}
  : ${NO_CONFIRM_REQUIRED=0}
}
#-------------------------------------------------------------------------------
# $1: env
# $2 user
# $3 title
#-------------------------------------------------------------------------------
function confirm_dialog() {
  local exitStatus
  local TITLE='Conferma'
  if [ "$3" != "" ]
  then
    TITLE="${TILE} $3"
  fi
  dialog \
    --backtitle "Gestione ambienti remoti" \
    --title "$TITLE" \
    --yesno "Confermi operazione remota su\n \n  [$1]\n \ncon utente\n \n  [$2]?" \
    12 70
  exitStatus=$?
  return $exitStatus
}

#-------------------------------------------------------------------------------
# $1: env
# $2 user
# $3 title
#-------------------------------------------------------------------------------
function ask_password() {
  local exitStatus
  local TITLE='Richiesta password'
  if [ "$3" != "" ]
  then
    TITLE="${TILE} $3"
  fi
  password=$(dialog \
    --backtitle "Gestione ambienti remoti" \
    --title "$TITLE" \
    --inputbox "Password per \n \n  [$1]\n \n utente\n \n  [$2]?" \
    12 70 \
    3>&2 2>&1 1>&3)
  exitStatus=$?
  if [ "$exitStatus" == ${DIALOG_OK} ]
  then
    echo $password
  else
    echo ""
  fi
  return $exitStatus
}


#
# Shows a menu with list of known SSH accesses
# SSH accesses must be set in bin/local/remote_users.dat
# see bin/local/remote_users.dat.sample for example file
#
function show_ssh_access_menu() {
  local MENUHEIGHT=0
  local MENUWIDTH=0
  local MENULINES=25
  local selected_value=""

  if [ "$1" == "" ]
  then
    TITLE='Operazioni su sistema remoto via SSH'
  else
    TITLE=$1
  fi

  ITEMS=$(cat ${SCRIPTDIR}/../local/remote_users.dat)
  W=()
  F=()
  while read -r line; do # process file by file
		let i=$i+1
		W+=($i "$line")
		F+=("$line")
	done < <( cat ${SCRIPTDIR}/../local/remote_users.dat )
	#
	SELECTION=$(dialog \
	  --backtitle "Gestione ambienti remoti" \
		--title "$TITLE" \
		--ok-label "Seleziona" \
		--cancel-label "Esci" \
		--menu "Seleziona il sistema remoto" ${MENUHEIGHT} ${MENUWIDTH} ${MENULINES} "${W[@]}" \
		3>&2 2>&1 1>&3) # show dialog and store output
	#
	exitStatus=$?
	#
	# get confirm execution
	#
	if [ "$exitStatus" == "$DIALOG_OK" ];
	then
    selected=${F[${SELECTION}-1]}
    # require confirm
	  if [ "$2" != "${NO_CONFIRM_REQUIRED}" ]
	  then
      dialog \
        --backtitle "Remote environment management" \
        --title "$TITLE" \
        --yesno "Confermi operazione remota su\n \n  ${selected_value[0]}\n \ncon utente\n \n  ${selected_value[1]}?" 12 70
      exitStatus=$?
    fi
	fi
	#
	echo $selected
	return $exitStatus
}
#
# check dialog exit status and call execution
#
function check_dialog_result() {
  case $exitStatus in
    $DIALOG_OK)
      clear
      execute_command
      ;;
    $DIALOG_CANCEL)
      clear
      echo "Annullato."
      ;;
    $DIALOG_ESC)
      clear
      echo "Annullato."
      ;;
  esac
}
#
# main script execution
#
function execute_script() {
  setup_dialog_codes
  show_params_dialog
  check_dialog_result
}
