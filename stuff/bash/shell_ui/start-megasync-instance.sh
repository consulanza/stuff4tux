#!/bin/bash
#-------------------------------------------------------------------------------
# Avvia un'istanza di MegaSync
# Copyright (C) 2015-2023 ConsuLanza Informatica
# Licenza MIT
#
# la procedura prevede che le istanza Mega siano ospitate tutte in una struttura
# definita come <home>/CloudStorage/Mega/<account email>
#
#
#-------------------------------------------------------------------------------
#
#------------------------------------------------------------------------------
# definizione costanti per codici di esecuzione dialog
#------------------------------------------------------------------------------
#
function setup_dialog_codes() {
  : ${DIALOG_OK=0}
  : ${DIALOG_CANCEL=1}
  : ${DIALOG_HELP=2}
  : ${DIALOG_EXTRA=3}
  : ${DIALOG_ITEM_HELP=4}
  : ${DIALOG_ESC=255}
}
#
#------------------------------------------------------------------------------
# recupera l'elenco delle istanze disponibili, genera il menu e lo visualizza
# per permettere la selezione
#------------------------------------------------------------------------------
#
function display_selection_menu() {
	echo ""
    	echo " Devi specificare l'istanza MEGA da attivare nella forma indirizzi@dominio.tlc"
    	echo " elenco istanze disponibili:"
	let i=0 # define counting variable
	W=() # define working array
	F=() # nomi file
	#
	# carica un array con l'elenco dei file, effettuando un ciclo sulle righe generate dal comando ls -1 (elenco
	# in colonna singola)
  #
	while read -r line; do # process file by file
		let i=$i+1
		W+=($i "$line")
		F+=("$line")
	done < <( ls -1 ${CLOUDROOT} )
	#
	# visualizza il menu tramite dialog
	#
	FILE=$(dialog \
		--title "Avvio istanza MEGASync" \
		--ok-label "Avvia" \
		--cancel-label "Annulla" \
		--menu "Seleziona l'istanza che vuoi avviare" 24 80 17 "${W[@]}" \
		3>&2 2>&1 1>&3) # show dialog and store output
	#
	# salva in exitStatus il codice di uscita di dialog
	#
	exitStatus=$?
	clear
	#
	# assegna il nome alla variabile INSTANCE
	#
	INSTANCE=${F[$FILE -1]}
}
#
#------------------------------------------------------------------------------
# elabora la richiesta da menu
# la variabile exitStatus è stata impostata dal comando dialog e ci indica se
# sia stato selezionato uno degli elementi o annullato tramite ESC o pulsante
# CANCEL
#------------------------------------------------------------------------------
#
function check_dialog_result() {
  case $exitStatus in
    $DIALOG_OK)
      clear
      execute_command
    ;;
    $DIALOG_CANCEL)
      clear
      echo "Annullato."
      exit
      ;;
    $DIALOG_ESC)
      clear
      echo "Annullato."
      exit
      ;;
  esac
}
#
#------------------------------------------------------------------------------
# esecuzione del comando per l'avvio dell'istanza MegaSync
#------------------------------------------------------------------------------
#
function execute_command() {
	#
	# se l'istanza specificata non esiste, segnala ed esce
	#
	if [ ! -d ${CLOUDROOT}/${INSTANCE} ]; then
	    echo "istanza '${INSTANCE}' non valida, devi specificare l'istanza tipo: indirizzo@dominio.it"
	    exit
	fi
	#
	HOME="${CLOUDROOT}/${INSTANCE}"
	#echo "home = ${HOME}"
	echo "Avvio MEGASync per ${HOME}"
	/usr/bin/megasync 2> /dev/null &
	HOME="/home/${USER}"
	#echo "home = $HOME"
	echo ""
}
#
#------------------------------------------------------------------------------
# MAIN - esecuzione procedura
# uso: start-megasync-instance.sh [nome]
# nome = nome istanza. Se non specificato la procedura provvede a generare un
# menu interattivo con l'elenco delle istanze disponibili.
#------------------------------------------------------------------------------
#
# assegna il primo parametro al nome istanza
INSTANCE="$1"
#
# calcola il percorso di base locale in base al nome utente
#
CLOUDROOT="/home/${USER}/CloudStorage/Mega"
#
if [ "${INSTANCE}" == "" ]; then
  # se non è stata indicata l'istanza da attivare, visualizza il menu delle
  # istanze disponibili e permette la selezione
  setup_dialog_codes
  display_selection_menu
  check_dialog_result
else
  # avvia il client per l'istanza selezionata
  execute_command
fi


