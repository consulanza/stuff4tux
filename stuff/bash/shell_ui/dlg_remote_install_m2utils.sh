#!/bin/bash
################################################################################
# Copyright (C) 2021 ConsuLanza Informatica
# All rights reserved
#
# Questa procedura mostra la finestra di dialogo per acquisire i parametri
# necessari all'installazione delle utility sul sistema remoto
################################################################################
#
SCRIPTDIR="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
source ${SCRIPTDIR}/dlg_functions.inc.sh
#
function show_params_dialog() {
  PROJECTNAME=''
  ENV_TYPE=''
  DEF_GIT=''
  # Store data to $VALUES variable
  VALUES=$(dialog \
    --backtitle "Remote environment management" \
    --title "Installa utility M2 su remoto" \
    --ok-label "Install M2 Utils" \
    --cancel-label "Annulla" \
    --form "Install M2 Utils on remote" \
    15 80 0 \
    "Project Name:"                               1 1	"$PROJECTNAME"  1 45 30 0 \
    "Environment (wrk, dev, prd, man):"           2 1	"$ENV_TYPE"  	  2 45 12 0 \
    "Default GIT branch: (wrk, develop, master)"  3 1	"$DEF_GIT"  	  3 45 12 0 \
    3>&2 2>&1 1>&3)
  # close fd
  exitStatus=$?
}
#
function execute_command() {
    clear
    echo "Installazione M2 Utils su sistema remoto ${PROJECTNAME}_${ENV_TYPE}"
    PROJECTNAME=${VALUES[0]}
    ENV_TYPE=${VALUES[1]}
    DEF_GIT=${VALUES[2]}
    ${SCRIPTDIR}/remote_install_m2utils.sh ${PROJECTNAME} ${ENV_TYPE} ${DEF_GIT}
}
#
execute_script
