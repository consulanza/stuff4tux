#!/bin/bash
################################################################################
# Copyright (C) 2021 ConsuLanza Informatica
# All rights reserved
#
# Questa procedura effettua l'installazione delle utility sul sistema remoto.
# deve essere prima creata la chiave RSA sull'host e deve essere stata
# configurata per l'accesso al repository
################################################################################
# create id_rsa on remote
#

GITPROJECT="user@githost.tld/utils"
#
# project name
PROJECTNAME=$1
# environment type
ENV_TYPE=$2
# default git branch
DEF_GIT=$3
# remote SSH user & server
REMOTE="${PROJECTNAME}_${ENV_TYPE}@${ENV_TYPE}.${PROJECTNAME}.${MAINDOMAIN}"

function install_n2utils_on_remote() {
    # clone GIT repository
    ssh ${REMOTE} "/usr/bin/mkdir -p ~/bin; cd ~/bin; echo "y" | git clone ssh://${GITPROJECT} ./"
    # setup env.sh
    ssh ${REMOTE} "/usr/bin/sed -e 's/#PROJECTNAME#/${PROJECTNAME}/' ~/bin/env/env.sh.sample | /usr/bin/sed -e 's/#ENV_TYPE#/${ENV_TYPE}/' | /usr/bin/sed -e 's/#DEF_GIT#/${DEF_GIT}/' > ~/env.sh"
    # copy bash profile
    ssh ${REMOTE} "/usr/bin/cp ~/bin/env/bash_profile.sample ~/.bash_profile"
    echo "Setup completed"
}

dialog \
  --backtitle "Remote environment management" \
  --title "Install M2 Utils on remote environment" \
  --yesno "Confermi l'installazione delle utility M2 su\n \n  ${REMOTE}\n \associato al ramo git\n \n  ${DEF_GIT}?" 12 70
#
# env.sh setup
#
response=$?
case $response in
   0)
     install_n2utils_on_remote
    ;;
    1) echo "Not confirmed";;
   255) echo "[ESC] key pressed.";;
esac
#
