#!/bin/bash
################################################################################
# Copyright (C) 2021 ConsuLanza Informatica
# All rights reserved
################################################################################
# remote call via ssh
# invoke git to update utils
#
REMOTE=$1
ssh $REMOTE "cd bin; /usr/bin/git pull"