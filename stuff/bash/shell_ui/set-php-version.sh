#!/bin/bash
#------------------------------------------------------------------------------
# Set current CLI PHP Version
# Copyright (C) 2021 Amedeo Lanza - ConsuLanza Informatica
# License: GPLv3
#------------------------------------------------------------------------------
#
MENUHEIGHT=20
MENUWIDTH=0
SELECTED_VERSION=""
#
PHPVER=`php -v`
#
exec 3>&1;
SELECTION=$(dialog \
--keep-tite \
--backtitle "${PHPVER}" \
--title "Selezione versione" \
--cancel-label "Annulla" \
--no-lines \
--menu "Seleziona La versione di PHP da utilizzare da linea comandi" ${MENUHEIGHT} ${MENUWIDTH} 50 \
0	"7.0" \
1	"7.1 - M2 2.3.0 -> 2.3.3" \
2	"7.2 - M2 2.3.1 -> 2.3.5" \
3	"7.3 - M2 2.3.3 -> 2.3.6" \
4	"7.4 - M2 2.3.7 -> 2.4.3" \
5	"8.1 - M2 2.4.4 -> 2.4.5" 2>&1 1>&3);

return_value=$?
exec 3>&-
#
dialog --clear
case $SELECTION in
    0)
    SELECTED_VERSION='7.0'
    echo "7.0"
    echo ""
    ;;
    1)
    SELECTED_VERSION='7.1'
    echo "7.1"
    echo ""
    ;;
    2)
    SELECTED_VERSION='7.2'
    echo "7.2"
    echo ""
    ;;
    3)
    SELECTED_VERSION='7.3'
    echo "7.3"
    echo ""
    ;;
    4)
    SELECTED_VERSION='7.4'
    echo "7.4"
    echo ""
    ;;
    5)
    SELECTED_VERSION='8.1'
    echo "8.1"
    echo ""
    ;;
esac
if [ "${SELECTED_VERSION}" = "" ]
then
    exit
fi
sudo update-alternatives --set php /usr/bin/php${SELECTED_VERSION}
sudo update-alternatives --set phar /usr/bin/phar${SELECTED_VERSION}
sudo update-alternatives --set phar.phar /usr/bin/phar.phar${SELECTED_VERSION}
