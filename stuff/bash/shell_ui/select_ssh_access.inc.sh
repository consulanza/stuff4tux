#!/bin/bash
################################################################################
# Copyright (C) 2021 ConsuLanza Informatica
# All rights reserved
################################################################################
#
# select ssh credentials for remote operation
# selected credentials must match a remote user with ssh key already enabled
# by ssh-copy-id
#
SCRIPTDIR="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
source ${SCRIPTDIR}/dlg_functions.inc.sh
#

#
function execute_command() {
  echo ${selected_value[0]}
  echo ${selected_value[1]}
}
#
setup_dialog_codes
show_ssh_access_menu "Test richiesta"
check_dialog_result