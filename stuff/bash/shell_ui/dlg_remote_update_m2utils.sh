#!/bin/bash
################################################################################
# Copyright (C) 2021 ConsuLanza Informatica
# All rights reserved
################################################################################
#
SCRIPTDIR="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
source ${SCRIPTDIR}/dlg_functions.inc.sh
#
function execute_command() {
  echo "Aggiornamento M2 Utils per ${selected_value[0]}"
  echo "Accesso remoto con utente ${selected_value[1]}"
  ${SCRIPTDIR}/remote_update_m2utils.sh ${selected_value[1]}
}
#
setup_dialog_codes
show_ssh_access_menu "Aggiornamento M2 Utils su remoto"
check_dialog_result