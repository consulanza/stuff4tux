#!/usr/bin/bash
#
# Define the dialog exit status codes
#
function setup_dialog_codes() {
  : ${DIALOG_OK=0}
  : ${DIALOG_CANCEL=1}
  : ${DIALOG_HELP=2}
  : ${DIALOG_EXTRA=3}
  : ${DIALOG_ITEM_HELP=4}
  : ${DIALOG_ESC=255}
  #
  : ${CONFIRM_REQUIRED=1}
  : ${NO_CONFIRM_REQUIRED=0}
}
#-------------------------------------------------------------------------------
# ask for password
#-------------------------------------------------------------------------------
function ask_password() {
    local exitStatus
    local TITLE='Richiesta password'
    if [ "$3" != "" ]
    then
      TITLE="${TILE} $3"
    fi
    password=$(dialog \
      --backtitle "Inserisci la password" \
      --title "$TITLE" \
      --inputbox "Password: " \
      12 70 \
      3>&2 2>&1 1>&3)
    exitStatus=$?
    if [ "$exitStatus" == ${DIALOG_OK} ]
    then
      echo $password
    else
      echo ""
    fi
    return $exitStatus
}
#-------------------------------------------------------------------------------
# check dialog exit status and call execution
#-------------------------------------------------------------------------------
function check_dialog_result() {
  case $exitStatus in
    $DIALOG_OK)
      clear
      execute_command
      ;;
    $DIALOG_CANCEL)
      clear
      echo "Annullato."
      ;;
    $DIALOG_ESC)
      clear
      echo "Annullato."
      ;;
  esac
}
#-------------------------------------------------------------------------------
# generic main script execution
#-------------------------------------------------------------------------------
function execute_script() {
  setup_dialog_codes
  show_dialog
  check_dialog_result
}
