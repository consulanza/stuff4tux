#!/bin/bash
################################################################################
# Copyright (C) 2021 ConsuLanza Informatica
# All rights reserved
################################################################################
# create id_rsa on remote
REMOTE=$1
ssh ${REMOTE} "/usr/bin/ssh-keygen; cat ~/.ssh/id_rsa.pub"
