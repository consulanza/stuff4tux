#!/bin/bash
################################################################################
# Copyright (C) 2021 ConsuLanza Informatica
# All rights reserved
#
# ripulisce il file di dump eliminando le istruzione DEFINER e sostituendo
# la codifica caratteri utf8mb4_0900_ai_ci con utf8mb4_general_ci
#
################################################################################
#
source ~/env.sh
source ~/bin/env/base_env.sh
source ~/bin/env/common_defines.sh
#
SED=/usr/bin/sed
#
if [ -z "$1" ]; then
  if [ "${DBNAME}" == "" ]; then
    DBNAME=${PROJECTNAME}_${ENV_TYPE}
  fi
else
  DBNAME=$1
fi
#
${SED} -e 's/DEFINER[ ]*=[ ]*[^*]*\*/\*/' ${DBNAME}.sql   | \
  ${SED} -e 's/DEFINER[ ]*=[ ]*[^*]*PROCEDURE/PROCEDURE/' | \
  ${SED} -e 's/DEFINER[ ]*=[ ]*[^*]*FUNCTION/FUNCTION/'   | \
  ${SED} -e 's/utf8mb4_0900_ai_ci/utf8mb4_general_ci/'      \
     > ${DBNAME}_no_definer.sql
