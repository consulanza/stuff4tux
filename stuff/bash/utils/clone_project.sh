#!/bin/bash
################################################################################
# Copyright (C) 2021 ConsuLanza Informatica
# All rights reserved
################################################################################
# clone project from repository
#
# skip any operation if git project already installad in webroot
# else
# rename webroot diretory to avoid any data loss
# create empty webroot
# clone project
#
source ~/env.sh
source ~/bin/env/base_env.sh
source ~/bin/env/common_defines.sh
#
function clone_git_project() {
  local GIT_PROJECT_URL=$1
  local LOCAL_GIT_DIR=$WEBROOT/.git
  #
  if [ -d "$LOCAL_GIT_DIR" ]; then
    echo "Directory GIT già presente:"
    cd $WEBROOT
    git branch -a
  else
    local TIMESTAMP=$(date +'%Y%m%d')
    echo "Clono il progetto."
    mv $WEBROOT ${WEBROOT}_${TIMESTAMP}
    mkdir -p $WEBROOT
    cd $WEBROOT
    pwd
    ls -al
    git clone ${GIT_PROJECT_URL} ./
    ls -al $WEBROOT
  fi
}
#
clone_git_project "$@"